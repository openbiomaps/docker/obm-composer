#!/bin/bash

cd /opt/openbiomaps

. ./determine_docker_compose.sh

./obm_pre_install.sh 80

$DOCKER_COMPOSE up -d chdir=/opt/openbiomaps && ./obm_post_install.sh
