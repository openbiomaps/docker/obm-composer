# obm-composer

Docker-composer for OpenBioMaps infrastructure


Find installation tutorial here: https://openbiomaps.org/documents/en/docker.html

Once, the docker environment is ready follow the instruction of new server install:

https://openbiomaps.org/documents/en/server_install.html


Simple install:

curl -s https://gitlab.com/openbiomaps/docker/obm-composer/-/raw/master/install.sh > /tmp/install.sh && sudo bash /tmp/install.sh
