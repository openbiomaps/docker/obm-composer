<?php
// Database connection definitions
define('gisdb_user','sablon_admin');
define('gisdb_pass','*** ChangeThisPassword ***');
define('gisdb_name','gisdata');
define('gisdb_host','gisdata'); //instead of localhost using docker's gisdata host

// Project's sql table name 
#define('PROJECTTABLE','your_database_table_name');
define('PROJECTTABLE',basename(__DIR__));

// Default project data restriction level
// 0 data read/modify for everybody
// 1 data read/modify only for logined users
// 2 data read/modify only for group members
// 3 data read/modify only own data
define('ACC_LEVEL',2);
define('MOD_LEVEL',2);

// Default language
define('LANG','en');

// Path settings
#define('OBMS','openbiomaps.org');
#define('OBMS',sprintf("%s",'localhost:9880/biomaps'));
define('PATH','/projects');
define('URL',sprintf("%s%s%s",$HOST,PATH,'/'.PROJECTTABLE));
define('OAUTHURL',sprintf("http://%s%s%s",'localhost',PATH,'/'.PROJECTTABLE));

// Mapserver's variables
define('PRIVATE_MAPSERV',sprintf("%s/private/proxy.php",URL));
define('PUBLIC_MAPSERV',sprintf("%s/public/proxy.php",URL));
define('PRIVATE_MAPCACHE',sprintf("%s/private/cache.php",URL));
define('PUBLIC_MAPCACHE',sprintf("%s/public/cache.php",URL));
define('MAPSERVER','http://mapserver/cgi-bin/mapserv');
define('MAPCACHE','http://mapserver/mapcache');
define('MAP','PMAP');
define('PRIVATE_MAPFILE','private.map');

// Invitations
// 0 Only admins can invite new members, any grater value the maximum active invitations per member
define('INVITATIONS',10);

// Mail settings
define('SMTP_AUTH',false); # true
define('SMTP_HOST','...');
define('SMTP_USERNAME','...');
define('SMTP_PASSWORD','...');
define('SMTP_SECURE','tls'); # ssl
define('SMTP_PORT','587'); # 465
define('SMTP_SENDER','openbiomaps@...');

// UI Settings
define('MAP_OVER_MAINPAGE',0);
define('LOAD_INTROPAGE',0);
define('SHINYURL',false);
define('RSERVER',false);
define('LOGINPAGE','map');
define('TRAINING',false);

define('MyHASH','*** ChangeThisHash ***');

// Deafult TimeZone if you need
date_default_timezone_set('Europe/Budapest');

// Docker specific constant
define('OB_PROJECT_DOMAIN',OB_DOMAIN);

// Project languages, the first is the default
define('LANGUAGES', array('en'=>'in English','hu'=>'magyarul','ro'=>'română','ru'=>'русский'));

// Max image size of attachments in bytes
define('ALLOWED_FILE_SIZE',4194304);

// A developer variable
// use the debug_upload file to define debug options:
// login-email  return-type project-table form-id
// obmdebugger@bugfixer.org return_error checkitout 473
//define('DEBUG_UPLOAD',false);

// Generate verbose pds logging to openbiomaps.log
//define('DEBUG_PDS',false);

// Don not use temporary_tables.obs_... table
define('USE_TEMPTABLES_FOR_OBSLISTS','false');
?>
