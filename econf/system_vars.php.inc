<?php
# obm app settings
define("OB_DOMAIN","localhost/biomaps");
#define("OB_SYSDIR","/var/lib/openbiomaps/"); // Path for OpenBioMaps system files, e.g. mapserver's access.key if it is in a non-default place; default is /var/lib/openbiomaps/
define("OB_TMP","/var/lib/openbiomaps/tmp/");
define("OB_ROOT","/var/www/html/biomaps/root-site/"); // Maybe you need to update this path. This is a directory which contains the `projects` directory - this constant used by the obm-application (afuncs, common_pg_funcs, ...)
define("OB_ROOT_SITE","/var/www/html/biomaps/root-site/"); // Maybe you need to update this path. This is a directory which contains the `supervisor.md` file - this constant only used by supervisor.php
define("OB_RESOURCES","/var/www/html/biomaps/resources/"); // Used by supervisor and create_new_project.php
#define("OB_WEB_PRE",""); // Used by the server_vars.php.inc sometimes the `biomaps` path is the correct value
#define("IN_DOCKER",true); // not used, but can be interesting?

define("PHP_PATH","/usr/local/bin/php"); // for docker

# Database access
define("POSTGRES_PORT","5432");
define("MAPSERVER_HOST",'mapserver');
#define('MAPSERVER_TMP','/tmp');

define('biomapsdb_user','biomapsadmin');
define('biomapsdb_pass','*** ChangeThisPassword-1 ***');
define('biomapsdb_name','biomaps');
define('biomapsdb_host','biomaps'); // instead of localhost using docker style standalone biomaps host
define('mainpage_user','mainpage_admin');
define('mainpage_pass','*** ChangeThisPassword-2 ***');

define('gisdata_dbs',array('gisdata')); // used by supervisor; list of gisdata databases
define('gisdb_hosts',array('biomaps_db')); // used by supervisor; list of host names for each databases
define("GISDB_HOST",'biomaps'); // Used by create-new-project; SHOULD BE REMOVE THIS to improve create new project on big servers...

# PostGIS version
define('POSTGIS_V','2.3');

# sendmail  or smtp
# If smtp, use SMTP_ variables in local_vars.php.inc files to enable per project smtp authentication
define('SENDMAIL','smtp');

# obm cache method
define('CACHE','memcache');

# R-Shiny Server ports
#define('RSERVER_PORT_sablon',7982);

# Bug report 
#define('AUTO_BUGREPORT_ADDRESS','');

# Computation
#define('COMPUTATIONAL_SERVERS',array(''));
#define('COMPUTATIONAL_CLIENT_SEC','');
#define('COMPUTATIONAL_CLIENT_KEY','');
#define('COMPUTATIONAL_CLIENT_NAME','');

?>
