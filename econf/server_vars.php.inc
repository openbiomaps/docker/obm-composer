<?php
# Server app's global variables
#
# Server app's default language
# define('LANG','hu');

# Institution running the server.
$institute_short_name = "OBM Gekko node";
$institute_logo = "favicon-120.png";

if (defined('OB_WEB_PRE'))
    $ob_web_pre = OB_WEB_PRE;
else
    $ob_web_pre = '';

# Access port
$this_port = isset($_SERVER['HTTP_X_FORWARDED_PORT']) ? $_SERVER['HTTP_X_FORWARDED_PORT'] : $_SERVER['SERVER_PORT'];
$this_host = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['SERVER_NAME'];
if ($this_port != 80 and $this_port != 443)
    $HOST = $this_host.":".$this_port.$ob_web_pre;
else
    $HOST = $this_host.$ob_web_pre;

?>
