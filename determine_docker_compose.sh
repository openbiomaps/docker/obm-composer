#!/bin/bash

# test against docker-compose V2 you can use the following command:
#

if [ -f docker-compose.yml ]; then
    if docker compose ps &>/dev/null; then
        DOCKER_COMPOSE="docker compose"
        export DOCKER_COMPOSE
        echo "Docker Compose V2 was used for existing containers"
    elif docker-compose ps &>/dev/null; then
        DOCKER_COMPOSE="docker-compose"
        export DOCKER_COMPOSE
        echo "Docker Compose V1 was used for existing containers"
    fi
fi

if [ -z "$DOCKER_COMPOSE" ]; then
    if docker compose version &>/dev/null; then
        DOCKER_COMPOSE="docker compose"
        export DOCKER_COMPOSE
        echo "The docker compose V2 is installed on this system"
    elif docker compose &>/dev/null && [ $? -eq 0 ]; then
        DOCKER_COMPOSE="docker compose"
        export DOCKER_COMPOSE
        echo "The docker compose V2 is installed on this system"
    elif command -v docker-compose &>/dev/null; then
        DOCKER_COMPOSE="docker-compose"
        export DOCKER_COMPOSE
        echo "The docker compose V1 is installed on this system"
    else
        echo "Neither Docker Compose V1 nor V2 is installed"
        exit 1
    fi
fi

echo "Docker Compose command set to: $DOCKER_COMPOSE"
