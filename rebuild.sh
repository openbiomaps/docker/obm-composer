#! /bin/bash

. ./determine_docker_compose.sh

SERVICE=$1
#$DOCKER_COMPOSE pull $SERVICE
$DOCKER_COMPOSE stop $SERVICE
$DOCKER_COMPOSE rm -v -f $SERVICE
$DOCKER_COMPOSE up -d $SERVICE
$DOCKER_COMPOSE logs -f $SERVICE
