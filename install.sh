#!/bin/bash

mkdir -p /srv/docker/openbiomaps && cd /srv/docker/openbiomaps

git clone https://gitlab.com/openbiomaps/docker/obm-composer.git .

. ./determine_docker_compose.sh

#groupadd -g 996 docker

#adduser foo docker

#docker network create obm_web

$DOCKER_COMPOSE pull

PORT=80

./obm_pre_install.sh $(true &>/dev/null </dev/tcp/127.0.0.1/$PORT && echo 9080)

$DOCKER_COMPOSE up -d

echo "Creating the gisdata database..."
res=''
while [[ -z "$res" ]];
do
    for s in / - \\ \|;
        do printf "\r$s"
        sleep 1
    done
    res=$($DOCKER_COMPOSE exec -T -u postgres biomaps_db psql postgres -c "SELECT datname FROM pg_catalog.pg_database;" | grep gisdata)
done

printf "\r"

res=''
while [[ -z "$res" ]];
do
    for s in / - \\ \|;
    do printf "\r$s"
        sleep 1
    done
    res=$($DOCKER_COMPOSE exec -T -u postgres biomaps_db psql gisdata -c "SELECT table_name FROM information_schema.tables;" | grep installation_complete)
done
# Wait for the docker gisdata is ready. Check the last database element exists....
sleep 5;

printf "\r"

echo "Setting up accesses..."
./obm_post_install.sh

echo "Restaring docker..."
$DOCKER_COMPOSE restart

echo -e "\nDone. Your OBM server is ready to use.\n"
echo $(true &>/dev/null </dev/tcp/127.0.0.1/$PORT && echo http://YOUR_SERVER_NAME:9080/ || echo http://YOUR_SERVER_NAME/)
